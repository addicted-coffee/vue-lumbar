import { Model, RestModel } from './lumbar'

export default class Book extends RestModel {
  static get attributes () {
    return ['id', 'title']
  }
}
