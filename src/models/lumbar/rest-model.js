import api from '@/services/api'
import Model from './model'

class RestModel extends Model {

  constructor () {
    super(...arguments)

    this.urlBase = this.constructor.urlBase
    this.httpActions = this.constructor.httpActions
  }

  static get urlBase () {
    return `/${this.resourceName()}`
  }

  static resourceName () {
    if (this.resourceNameCache) return this.resourceNameCache
    this.resourceNameCache = `${this.namespace}s`
    return this.resourceNameCache
  }

  static async find (id) {
    let instance = super.find(id)
    if (instance) return instance

    const { data } = await api.get(`${this.urlBase}/${id}`)

    return this.load(data.data)
  }

  // TODO: pagination
  static async fetch () {
    const { data } = await api.get(`${this.urlBase}/`)

    data.data.forEach((entry) => {
      this.load(entry)
    })

    return this.all()
  }

  async preserve () {
    let attributes = this.attributes()
    if (!this.id) {
      const response = await api.post(`${this.urlBase}/`, attributes)
      this.changeID(response.data.data.id)
    } else {
      const response = await api.patch(`${this.urlBase}/${this.id}`, attributes)
    }
  }

  save () {
    this.preserve()
  }

  async destroy () {
    await this.vm.$http.delete(`${this.urlBase}/${this.id}`)
    this.delete()
  }
}

export default RestModel
