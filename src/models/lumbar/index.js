import Model from './model'
import RestModel from './rest-model'
import storeModule from './store-module'

export {
  Model,
  RestModel,
  storeModule,
}
