import storeModule from './store-module'

export default class Model {
  constructor (attrs = {}) {
    this.store = this.constructor.store
    this.vm = this.constructor.store.vm
    this.namespace = this.constructor.namespace
    this.properties = this.constructor.attributes.slice()

    this.properties.forEach(property => {
      if (attrs[property.name]) return
      // TODO: default values?
      attrs[property.name] = null
    })
  }

  static get attributes () {
    return this.attributes()
  }

  static find (id) {
    return this.getter('find', id)
  }

  static findAll (ids) {
    return this.getter('findAll', ids)
  }

  static exists (id) {
    return this.getter('exists', id)
  }

  static select (fn) {
    return this.getter('select', fn)
  }

  static findBy (name, value) {
    return this.getter('findBy', name, value)
  }

  static findAllBy (name, value) {
    return this.getter('findAllBy', name, value)
  }

  static all () {
    return this.getter('all')
  }

  static deleteAll () {
    this.mutation('deleteAll')
  }

  static updateRecord (attributes) {
    this.mutation('updateRecord', attributes)
  }

  static addRecord (attributes) {
    this.mutation('addRecord', attributes)
  }

  static load (attributes) {
    // we need to update existing records
    // otherwise they will loose their reactivity
    if (this.exists(attributes.id)) {
      this.updateRecord(attributes)
    } else {
      this.addRecord(attributes)
    }
    return this.find(attributes.id)
  }

  static delete (id) {
    this.mutation('delete', id)
  }

  changeID (id) {
    this.mutation('updateRecord', {
      id: id,
    })
  }

  // private

  static mutation (mutation, parameter) {
    this.store.commit(`${this.namespace}/${mutation}`, parameter)
  }

  static getter (getter) {
    // remove first argument (getter) to get real arguments
    const args = Array.prototype.slice.call(arguments, 1)
    return this.store.getters[`${this.namespace}/${getter}`](...args)
  }

  static registerStore (store, namespace) {
    this.store = store
    this.namespace = namespace

    if (store.state[namespace]) return

    this.store.registerModule(namespace, storeModule)
  }

  attributes () {
    var result = {}

    this.properties.forEach(property => {
      result[property] = this[property]
    })

    return result
  }

  mutation () {
    this.constructor.mutation(...arguments)
  }

  getter () {
    return this.constructor.getter(...arguments)
  }
}
