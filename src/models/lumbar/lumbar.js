export default {
  install (Vue, { store, models }) {
    Object.keys(models).forEach(namespace => {
      let ModelClass = models[namespace]
      ModelClass.registerStore(store, namespace)
    })
  }
}
