import app from '../main'
import { create } from 'apisauce'
import store from '@/store'
import router from '@/router'


const api = create({
  baseURL: process.env.API_URL
})

api.addRequestTransform(() => {
  if (app !== undefined) {
    app.$Progress.start()
  }
})

api.addResponseTransform(() => {
  if (app !== undefined) {
    app.$Progress.finish()
  }
})

export default api
