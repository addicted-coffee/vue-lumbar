// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from "@/store"
import Book from "@/models/Book"

import _ from 'lodash'

import Lumbar from '@/models/lumbar/lumbar.js'
Vue.use(Lumbar, {
  store: store,
  models: {
    book: Book
  }
})

// Vue Progress Bar
import VueProgressBar from "vue-progressbar"
Vue.use(VueProgressBar, {
  color: "#41b883",
  thickness: "6px"
})


Vue.config.productionTip = false

/* eslint-disable no-new */
export default new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
